import cvxpy as cvx
import pandas as pd
import numpy as np
import difflib

import odds
import holdet

def name_alignment(df1, df2, column):
    # Fix names
    no_matches = []
    for n in range(0, len(df2)):
        matches = difflib.get_close_matches(df2[column][n], df1[column])

        if matches:
            # Assign best match
            df2.at[n, column] = matches[0]
        else:
            no_matches.append(n)
    
    return df2.drop(df2.index[no_matches]).groupby(column).sum()

def get_weights(stage):
    stats = holdet.load_statistics(stage)
    probs = odds.load_odds(stage)

    probs = name_alignment(stats, probs, 'name')
    
    weights = probs.columns
    
    probs['name'] = probs.index
    stats = pd.merge(stats, probs, on='name', how='outer')

    stats.fillna(np.inf, inplace=True)

    # Re-adjust for a maximization problem, and calc. scalings:
    scale = {}
    for weight in weights:
        stats[weight] = 1/stats[weight]
        scale[weight] = stats[weight].astype(bool).sum() / stats[weight].sum()

    stats = stats.sort_values('name').reset_index()

    return stats, scale

def update_team(stage, value, prev_team = None, n_exchanges = None, initial_cost = False):
    stats, scale = get_weights(stage)

    # Our team selection variable
    x = cvx.Variable(len(stats), boolean=True)

    cost = cvx.Variable()

    P_any_stage = cvx.Variable()
    P_kom_win = cvx.Variable()
    P_point_win = cvx.Variable()
    P_win = cvx.Variable()
    P_stage = cvx.Variable()

    if initial_cost == True:
        cost_expr = (
            scale['w_win'] * P_win + 
            scale['w_any_stage'] * P_any_stage + 
            scale['w_stage_win'] * P_stage + 
            scale['w_point_win'] * P_point_win
        )
    else:
        cost_expr = (
            scale['w_win'] * P_win + 
            #scale['w_any_stage'] * P_any_stage + 
            0.75 * scale['w_stage_win'] * P_stage
        )

    objective = cvx.Maximize(
        cost_expr
    )
  
    constraints = []
    
    constraints.append(P_any_stage == stats['w_any_stage'].values @ x)
    constraints.append(P_kom_win == stats['w_kom_win'].values @ x)
    constraints.append(P_point_win == stats['w_point_win'].values @ x)
    constraints.append(P_win == stats['w_win'].values @ x)
    constraints.append(P_stage == stats['w_stage_win'].values @ x)

    # Only use n_exchanges
    if n_exchanges != None:
        constraints.append(cvx.norm(prev_team - x, 1) / 2 <= n_exchanges + 0.1)

    constraints.append(cost == stats['value'].values @ x)

    constraints.append(cost <= value)
    constraints.append(sum(x) == 8)

    problem = cvx.Problem(objective, constraints)
    result = problem.solve()

    team = {}
    team['riders'] = np.round(x.value)
    team['riders_index'] = np.where(team['riders'] == 1)[0]

    captain_selection = stats['w_stage_win'][np.where(x.value > 0.99)[0]]

    team['captain'] = np.zeros(len(stats))
    team['captain'][captain_selection.idxmax()] = 1
    
    return team

def update_team2(stage, value, prev_team = None, n_exchanges = None, initial_cost = False):
    stats, scale = get_weights(stage)

    # Our team selection variable
    x = cvx.Variable(len(stats), boolean=True)

    cost = cvx.Variable()

    #P_any_stage = cvx.Variable()
    P_kom_win = cvx.Variable()
    P_point_win = cvx.Variable()
    P_win = cvx.Variable()
    P_stage = cvx.Variable()

    if initial_cost == True:
        cost_expr = (
            scale['w_win'] * P_win + 
            scale['w_any_stage'] * P_any_stage + 
            scale['w_stage_win'] * P_stage + 
            scale['w_point_win'] * P_point_win
        )
    else:
        cost_expr = (
            scale['w_win'] * P_win + 
            #scale['w_any_stage'] * P_any_stage + 
            1 * scale['w_stage_win'] * P_stage
        )

    objective = cvx.Maximize(
        cost_expr
    )
  
    constraints = []
    
    #constraints.append(P_any_stage == stats['w_any_stage'].values @ x)
    constraints.append(P_kom_win == stats['w_kom_win'].values @ x)
    constraints.append(P_point_win == stats['w_point_win'].values @ x)
    constraints.append(P_win == stats['w_win'].values @ x)
    constraints.append(P_stage == stats['w_stage_win'].values @ x)

    # Only use n_exchanges
    if n_exchanges != None:
        constraints.append(cvx.norm(prev_team - x, 1) / 2 <= n_exchanges + 0.1)

    constraints.append(cost == stats['value'].values @ x)

    constraints.append(cost <= value)
    constraints.append(sum(x) == 8)

    problem = cvx.Problem(objective, constraints)
    result = problem.solve()

    team = {}
    team['riders'] = np.round(x.value)
    team['riders_index'] = np.where(team['riders'] == 1)[0]

    captain_selection = stats['w_stage_win'][np.where(x.value > 0.99)[0]]

    team['captain'] = np.zeros(len(stats))
    team['captain'][captain_selection.idxmax()] = 1
    
    return team

def initial_team():
    return update_team(0, 50e6, initial_cost=True)

if __name__ == '__main__':
    team0 = initial_team()