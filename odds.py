import bs4
import requests
import pandas as pd
import numpy as np

base_url = 'https://www.oddschecker.com/cycling/tour-de-france'

def odds_scrape(url, tag):

    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0'}
    resp = requests.get(url, headers=headers)

    soup = bs4.BeautifulSoup(resp.content, 'html.parser')

    rows = soup.select('tbody > tr')

    data = []
    for row in rows:
        # Grab all column entries
        data.append({
            'name': row.attrs['data-bname'], 
            tag: float(row.attrs['data-best-dig'])
        })


    return pd.DataFrame(data)

def stage_win(stage):
    url = base_url + '/tour-de-france-stage-{0}/winner'.format(stage)
    return odds_scrape(url, 'w_stage_win')

def stage_top3(stage):
    url = base_url + '/tour-de-france-stage-{0}/top-3-finish'.format(stage)
    return odds_scrape(url, 'w_stage_top3')

def stage_top10(stage):
    url = base_url + '/tour-de-france-stage-{0}/top-10-finish'.format(stage)
    return odds_scrape(url, 'w_stage_top10')

def kom_win():
    url = base_url + '/king-of-the-mountains'
    return odds_scrape(url, 'w_kom_win')

def point_win():
    url = base_url + '/points-classification'
    return odds_scrape(url, 'w_point_win')

def win():
    url = base_url + '/winner'
    return odds_scrape(url, 'w_win')

def win_top3():
    url = base_url + '/top-10-finish'
    return odds_scrape(url, 'w_top3')

def win_top10():
    url = base_url + '/top-3-finish'
    return odds_scrape(url, 'w_top10')

def any_stage_win():
    url = base_url + '/to-win-any-stage-of-the-tour-de-france-2018'
    return odds_scrape(url, 'w_any_stage')

def get_all(stage):
    odds = [
        stage_win(stage+1),
        kom_win(), point_win(),# any_stage_win(),
        win(), win_top3()#, win_top10()
    ]

    all_odds = odds[0]
    odds.pop(0)

    for df_i in odds:
        all_odds = pd.merge(all_odds, df_i, on='name', how='outer')

    all_odds = all_odds.fillna(np.inf)

    store = pd.HDFStore('oddschecker_dataset.h5')
    store.put('stage_' + str(stage), all_odds)
    store.close()

    return all_odds

def load_odds(stage):
    return pd.read_hdf('oddschecker_dataset.h5', 'stage_' + str(stage))