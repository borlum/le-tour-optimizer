import bs4
import requests
import pandas as pd

def get_statistics(stage):
    pages = range(0,8)
    url = "https://www.holdet.dk/da/tour-de-france-2019/statistics/{0}?&page={1}"

    data = []
    
    for page in pages:

        resp = requests.get(url.format(stage, page))
        soup = bs4.BeautifulSoup(resp.content, 'html.parser')
        rows = soup.select('tbody > tr')

        for row in rows:
            # Grab all column entries
            cols = row.select('td')

            # Helper: strip newlines
            strip = lambda str: "0" if str.lstrip().rstrip() == "-" else str.lstrip().rstrip()

            # Grab data for each rider
            rider = {}
            
            rider['id'] = cols[0].get_text()
            rider['name'] = cols[1].select('a')[0].get_text()
            rider['team'] = cols[1].select('div')[0].get_text()[:-9]
            
            # Remove 1000th separator
            rider['value'] = int( strip(cols[2].get_text()).replace('.', '') )
            rider['growth'] = int( strip(cols[3].get_text()).replace('.', '') )
            rider['growth_tot'] = int( strip(cols[4].get_text()).replace('.', '') )
            
            rider['gc'] = int(strip(cols[5].get_text()))
            rider['sprint'] = int(strip(cols[6].get_text()))
            rider['mountain'] = int(strip(cols[7].get_text()))
            
            rider['stage'] = int(strip(cols[8].get_text()))
            
            rider['sprint_points'] = int(strip(cols[9].get_text()))
            rider['mountain_points'] = int(strip(cols[10].get_text()))
            
            rider['popularity'] = float(strip(cols[11].get_text())[:-1].replace(',', '.'))
            rider['trend'] = float(strip(cols[12].get_text()))
            
            data.append(rider)


    stats = pd.DataFrame(data)

    print(stats)
    store = pd.HDFStore('holdetdk_dataset.h5')
    store.put('stage_' + str(stage), stats)
    store.close()

def load_statistics(stage):
    return pd.read_hdf('holdetdk_dataset.h5', 'stage_' + str(stage))