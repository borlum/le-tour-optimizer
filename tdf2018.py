import numpy as np
import pandas as pd
import holdet

# State number
n = 0

# Get data
stats = holdet.load_statistics(n)
N_R = len(stats)

teams = stats.groupby(['team']).size()
N_T = len(teams)

# Rider to team transform
R_T = stats.pivot(index=stats.index, columns='team')['id'].fillna(0).astype(bool).astype(int)

# Construct stage class. weight
w_c = [
    200e3, 150e3, 130e3, 120e3, 110e3, 100e3, 95e3, 
    90e3, 85e3, 80e3, 70e3, 55e3, 40e3, 30e3, 15e3
]
w_c = np.array(w_c + (N_R - len(w_c)) * [0])

# Construct team stage class. weight
w_c_T = [100e3, 50e3, 30e3]
w_c_T = np.array(w_c_T + (N_T - len(w_c_T)) * [0])

# Construct overall class. weight
w_c_tot = [100e3, 90e3, 80e3, 70e3, 60e3, 50e3, 40e3, 30e3, 20e3, 10e3]
w_c_tot = np.array(w_c_tot + (N_R - len(w_c_tot)) * [0])

# Construct point obtained weight
w_p = 3000

# Construct jersey weight
w_j = 25e3

# Captain bonus
w_cap = 2

# Stage bonus
G_s = lambda N_s: int(2195.01 * np.exp(0.588975 * N_s))
c_s = np.array([1] * 15 + [0] * (N_R - 15))

def F(x, team, results):
    x_B = x[0]
    x_R = x[1:]

    results['stage_classification_team'] = results['stage_classification'] @ R_T

    # Rider growth
    growth = (
        results['stage_classification'] @ w_c + 
        results['stage_classification_team'] @ w_c_T + 
        results['points_obtained'] * w_p + 
        results['jersey_held'] * w_j + 
        results['overall_classfication'] @ w_c_tot
    )

    # Rider update
    x_R = x_R + growth

    # Bank update
    x_B = (
        x_B + 
        w_cap * (team['captain'] @ growth) + 
        G_s((team['riders'] @ results['stage_classification']) @ c_s) + 
        0.05 * x_B
    )
    
    return np.append(x_B, x_R)

if __name__ == '__main__':
    
    res = {}
    # Construct stage class. result
    res['stage_classification'] = np.diag(np.ones(N_R))

    # Construct overall class. result
    res['overall_classfication'] = np.diag(np.ones(N_R))

    # Construct point obtained result
    res['points_obtained'] = np.zeros(N_R)

    res['points_obtained'][49] = 7
    res['points_obtained'][42] = 3

    # Construct jersey results
    res['jersey_held'] = np.zeros(N_R)

    res['jersey_held'][23] = 1
    res['jersey_held'][78] = 1
    res['jersey_held'][100] = 1

    # Team specific
    team = {}

    team['riders'] = np.zeros(N_R)
    team['riders'][47] = 1
    team['riders'][48] = 1
    team['riders'][49] = 1
    team['riders'][50] = 1
    team['riders'][51] = 1
    team['riders'][52] = 1
    team['riders'][53] = 1
    team['riders'][54] = 1

    team['captain'] = np.zeros(N_R)
    team['captain'][49] = 1

    # Get initial value for rider value state
    x_R_0 = stats['value'].values

    # Get initial bank
    x_B_0 = [0]

    x_0 = np.append(x_B_0, x_R_0)

    print(F(x_0, team, res))